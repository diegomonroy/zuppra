<!-- Begin Top -->
	<section class="top_1" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_1' ); ?>
			</div>
		</div>
	</section>
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns text-center">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
		</div>
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->