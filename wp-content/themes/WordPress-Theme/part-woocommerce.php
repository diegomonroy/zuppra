<?php
$left = 'left';
$column = '';
$banner = '';
if ( ! is_product() ) {
	$column = 'medium-9';
}
/* Accesorios */
$category_1_id = 67;
$category_1 = 'accesorios';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_accesorios';
		$column = 'medium-9';
		$banner = 'banner_accesorios';
	}
}
/* Bicicletas */
$category_1_id = 17;
$category_1 = 'bicicletas';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_bicicletas';
		$column = 'medium-9';
		$banner = 'banner_bicicletas';
	}
}
/* Componentes */
$category_1_id = 26;
$category_1 = 'componentes';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_componentes';
		$column = 'medium-9';
		$banner = 'banner_componentes';
	}
}
/* Marcos */
$category_1_id = 62;
$category_1 = 'marcos';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_marcos';
		$column = 'medium-9';
		$banner = 'banner_marcos';
	}
}
/* Protecciones */
$category_1_id = 42;
$category_1 = 'protecciones';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_protecciones';
		$column = 'medium-9';
		$banner = 'banner_protecciones';
	}
}
/* Ropa */
$category_1_id = 50;
$category_1 = 'ropa';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_ropa';
		$column = 'medium-9';
		$banner = 'banner_ropa';
	}
}
/* Zapatos */
$category_1_id = 58;
$category_1 = 'zapatos';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_zapatos';
		$column = 'medium-9';
		$banner = 'banner_zapatos';
	}
}
?>
<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( $banner ); ?>
			</div>
		</div>
	</section>
<!-- End Banner -->
<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row">
			<?php if ( ! is_product() ) : ?>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( $left ); ?>
			</div>
			<?php endif; ?>
			<div class="small-12 <?php echo $column; ?> columns">
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->